import request from "../utils/request"
import constantName from '../utils/constant'
// import axios from 'axios'
// 获取
export const getBigScreenInfo = (params) => {
    return request({
      url: constantName.api_big_screen + "/getInfo",
      method: "get",
      params: params
    })
}
// 获取
export const getBigScreenList = (params) => {
  return request({
    url: constantName.api_big_screen + "/getList",
    method: "get",
    params: params
  })
}

import request from "../utils/request"
import constantName from '../utils/constant'
// getCode
export const getCode = (params) => {
    return request({
        url: constantName.api_captcha + "/captchaImage",
        method: "get"
    })
}
// login
export const login = (params) => {
    return request({
        url: constantName.api_login + "/login",
        method: "post",
        data: params
    })
}
export const logout = () => {
    return request({
        url: constantName.api_login + "/logout",
        method: "post"
    })
}
// 获取
export const getInfo = (params) => {
    return request({
        url: constantName.api_login + "/getInfo",
        method: "get",
        params: params
    })
}

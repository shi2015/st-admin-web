import request from "../utils/request"
import constantName from '../utils/constant'
// getMenus
export const getMenus = () => {
    return request({
      url: constantName.api_login + "/getMenus",
      method: "get",
    })
}

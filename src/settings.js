module.exports = {
  /**
   * @description 标题
   */
  title: 'ST-ADMIN 后台管理',
  /**
   * @description 记住密码状态下的token在Cookie中存储的天数，默认1天
   */
  tokenCookieExpires: 1,
  /**
   * @description 记住密码状态下的密码在Cookie中存储的天数，默认1天s
   */
  passCookieExpires: 1,
  /**
   * @description token key
   */
  TokenKey: 'ST-ADMIN-TOEKN',
  /**
   * @description 请求超时时间，毫秒（默认1分钟）
   */
  timeout: 600000,
  /**
   * 底部文字，支持html语法
   */
  footerTxt: '© 2020 xxx <a href="www.baidu.com" target="_blank">xxx</a>',
}

import { createApp } from 'vue'
import app from './App.vue'
import router from './router'
import Antd from 'ant-design-vue/es';
import { message } from 'ant-design-vue/es';
import 'ant-design-vue/dist/antd.less';
import * as Icons from "@ant-design/icons-vue";
import '@/assets/iconfonts/iconfont.css';
import dataV from '@jiaminghi/data-view'
import store from './store'
import Bus from '@/common/bus.js'/// mitt 总线程引入
import Config from '@/settings'
// 适配flex
import '@/common/flexible.js';
//引入echart
import echarts from 'echarts'

//require('./mock');

const App = createApp(app,{});
App.use(router);
App.use(Antd);
App.use(dataV);
App.use(store);
/* 代替 Vue.prototype */
App.config.globalProperties.$echarts = echarts
App.config.globalProperties.$bus = Bus;
App.config.globalProperties.$Config = Config;
App.config.globalProperties.$message = message;
//引用
for (const i in Icons) {
    App.component(i, Icons[i]);
}
App.mount('#app');


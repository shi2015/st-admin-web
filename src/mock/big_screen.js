/* eslint-disable no-dupe-keys */
let big_screen_info = {
    code: 200,
    message: 'success',
    data: {
        // 50至100以内随机整数, 70只是用来确定类型
        'num1|50-100': 70,
        'num2|50-100': 70,
        'num3|50-100': 70,
        'num4|50-100': 70,
        'num5|50-100': 70,
        'num6|50-100': 70
    }
};
// noinspection JSDuplicatedDeclaration
let list = {
    'get|/bigScreen/getInfo': big_screen_info,
    // 也可以这样写
    // 官方解释为：记录用于生成响应数据的函数。当拦截到匹配 rurl 和 rtype 的 Ajax 请求时，函数 function(options) 将被执行，并把执行结果作为响应数据返回。
    // eslint-disable-next-line no-unused-vars
    "get|/bigScreen/getList": (option) => {
        // 可以在这个地方对demoList2进行一系列操作，例如增删改
        // option 指向本次请求的 Ajax 选项集，含有 url、type 和 body 三个属性
        return {
            code: 200,
            message: 'success',
            'data|1-10': [{
                // 属性 RegisterNo 是一个自增数，起始值为 1，每次增 1
                'RegisterNo|+1': 1,
                // 随机数字1-100
                'reading|1-100': 100,
                'ElectricityQuantity|1-100': 100,
                'name': '@cname',  // 中文名称
                'date': '@date("yyyy-MM-dd")',  // 日期
                'city': '@city(true)',   // 中国城市
                'color': '@color',  // 16进制颜色
                'isMale|1': true,  // 布尔值
                'isFat|1-2': true,  // true的概率是1/3
                'brother|1': ['jack', 'jim'], // 随机选取 1 个元素
            }]
        };
    }
};
export default list

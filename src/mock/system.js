/* eslint-disable no-dupe-keys */
let user_info = {
    code: 200,
    message: 'success',
    data: {
        'name': '@cname',  // 中文名称
    }
};
// noinspection JSDuplicatedDeclaration
let list = {
    'get|/user/getInfo': user_info,
    // eslint-disable-next-line no-unused-vars
    "get|/user/getMenus": (option) => {
        return {
            code: 200,
            message: 'success',
            data: [{
                id: 1,
                parentId: 0,
                title: "首页",
                name: "01",
                icon: "icon-home",
                level: 1,
                sort: 1,
                children:[{
                    id: 111,
                    parentId: 3,
                    title: "首页",
                    name: "0101",
                    icon: "icon-home",
                    path: "/",
                    level: 2,
                    sort: 1,
                    children:[]
                },{
                    id: 112,
                    parentId: 3,
                    title: "地图",
                    name: "0102",
                    icon: "icon-home",
                    path: "/baidumap/",
                    level: 2,
                    sort: 1,
                    children:[]
                },{
                    id: 113,
                    parentId: 3,
                    title: "大屏1",
                    name: "0103",
                    icon: "icon-home",
                    path: "/big_screen_main/",
                    level: 2,
                    sort: 1,
                    children:[]
                },{
                    id: 114,
                    parentId: 3,
                    title: "大屏2",
                    name: "0104",
                    icon: "icon-home",
                    path: "/big_screen_main1/",
                    level: 2,
                    sort: 1,
                    children:[]
                }]
            }, {
                id: 3,
                parentId: 0,
                title: "数据监控",
                name: "03",
                icon: "icon-zhuzhuangtu",
                level: 1,
                sort: 2,
                children:[{
                    id: 9,
                    parentId: 3,
                    title: "系统数据",
                    name: "0301",
                    icon: "icon-chaobiaoxitong1",
                    level: 2,
                    sort: 1,
                    children: [{
                        id: 27,
                        parentId: 9,
                        title: "在线用户",
                        name: "030101",
                        path: "/monitor/",
                        component: "summaryData",
                        icon: "icon-shengchanbaobiao",
                        level: 3,
                        sort: 1,
                        children: []
                    }, {
                        id: 28,
                        parentId: 9,
                        title: "操作日志",
                        name: "030102",
                        path: "/monitor/",
                        component: "householdData",
                        icon: "icon-shengchanbaobiao",
                        level: 3,
                        sort: 2,
                        children: []
                    }, {
                        id: 29,
                        parentId: 9,
                        title: "异常日志",
                        name: "030103",
                        path: "/monitor/",
                        component: "householdData",
                        icon: "icon-shengchanbaobiao",
                        level: 3,
                        sort: 2,
                        children: []
                    }]
                }, {
                    id: 45,
                    parentId: 3,
                    title: "服务监控",
                    name: "0302",
                    path: "",
                    component: "",
                    icon: "icon-lishishuju",
                    level: 2,
                    sort: 2,
                    children: [{
                        id: 46,
                        parentId: 45,
                        title: "服务监控",
                        name: "030201",
                        path: "/history/",
                        component: "summaryHistoryData",
                        icon: "icon-lishishuju",
                        level: 3,
                        sort: 1,
                        children: []
                    }]
                }]
            }, {
                id: 4,
                parentId: 0,
                title: "统计分析",
                name: "04",
                icon: "icon-yujingfenxi",
                level: 1,
                sort: 3,
                children:[{
                    id: 12,
                    parentId: 4,
                    title: "预警信息",
                    name: "0401",
                    path: "/alarm/",
                    component: "terminalFault",
                    icon: "icon-xitongbaojing",
                    level: 2,
                    sort: 1,
                    children: []
                }, {
                    id: 50,
                    parentId: 4,
                    title: "xxx信息",
                    name: "0402",
                    path: "/record/",
                    component: "changeTerminal",
                    icon: "icon-shengchanguanli",
                    level: 2,
                    sort: 2,
                    children: []
                }]
            }, {
                id: 5,
                parentId: 0,
                title: "系统设置",
                name: "05",
                icon: "icon-setting",
                level: 1,
                sort: 5,
                children:[{
                    id: 13,
                    parentId: 5,
                    title: "用户管理",
                    name: "0501",
                    icon: "icon-setting",
                    level: 2,
                    sort: 1,
                    children: [{
                        id: 37,
                        parentId: 13,
                        title: "用户管理",
                        name: "050101",
                        path: "/system/",
                        component: "user",
                        icon: "icon-filesearch",
                        level: 3,
                        sort: 1,
                        children: []
                    }, {
                        id: 38,
                        parentId: 13,
                        title: "角色管理",
                        name: "050102",
                        path: "/system/",
                        component: "role",
                        icon: "icon-filesearch",
                        level: 3,
                        sort: 2,
                        children: []
                    }, {
                        id: 39,
                        parentId: 13,
                        title: "部门管理",
                        name: "050102",
                        path: "/system/",
                        component: "role",
                        icon: "icon-filesearch",
                        level: 3,
                        sort: 2,
                        children: []
                    }, {
                        id: 40,
                        parentId: 13,
                        title: "岗位管理",
                        name: "050103",
                        path: "/system/",
                        component: "role",
                        icon: "icon-filesearch",
                        level: 3,
                        sort: 2,
                        children: []
                    }]
                }, {
                    id: 14,
                    parentId: 5,
                    title: "系统设置",
                    name: "0502",
                    icon: "icon-menu",
                    level: 2,
                    sort: 2,
                    children: [{
                        id: 39,
                        parentId: 14,
                        title: "菜单管理",
                        name: "050201",
                        path: "/system/",
                        component: "menus",
                        icon: "icon-menu",
                        level: 3,
                        sort: 1,
                        children: []
                    },{
                        id: 40,
                        parentId: 14,
                        title: "字典管理",
                        name: "050203",
                        path: "/system/",
                        component: "menus",
                        icon: "icon-menu",
                        level: 3,
                        sort: 1,
                        children: []
                    },{
                        id: 41,
                        parentId: 14,
                        title: "定时任务",
                        name: "050204",
                        path: "/system/",
                        component: "menus",
                        icon: "icon-menu",
                        level: 3,
                        sort: 1,
                        children: []
                    }]
                }]
            }]

        };
    }
};
export default list

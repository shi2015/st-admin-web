import {loadView} from "../router";

const getters = {
  token: state => state.user.token,
  roles: state => state.user.roles,
  user: state => state.user.user,
  loadMenus: state => state.menu.loadMenus,
  collapsed: state => state.collapsed,
  menu_routers: state => {
    console.log(state.menu.routers)
    return state.menu.routers.filter(router => {
      if (router && !router.hidden) {
        return true
      } else {
        return false
      }
    })
  },
}
export default getters

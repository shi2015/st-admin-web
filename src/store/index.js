import { createStore } from 'vuex'
import getters from './getters'
const modulesFiles = require.context('./modules', true, /\.js$/)

// 也可以这样引用 `import user from './modules/user'`
// 自动引用modules下的文件
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  // set './app.js' => 'app'
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})

const store = createStore({
  state: {
    collapsed: false,
  },
  mutations: {
    SET_COLLAPSED: (state, val) => {
      state.collapsed = val
    }
  },
  actions: {
  },
  modules,
  getters
})

export default store

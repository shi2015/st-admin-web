/* eslint-disable */
// 本地服务地址
const local_service_url = '/api'
// 生产环境服务地址
const prod_service_url = 'http://localhost:8001/rest'

// 接口地址
// 大屏接口
const api_big_screen = '/bigScreen'
const api_user = '/user'
const api_captcha = '/captcha'
const api_login = '/login'

export default{
  local_service_url,
  prod_service_url,
  api_big_screen,
  api_user,
  api_captcha,
  api_login
}

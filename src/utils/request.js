import axios from 'axios'
import constantName from '@/utils/constant'
// import store from '@/store'
import { getToken } from '@/utils/auth'

// 创建axios实例
const service = axios.create({
  /*headers: {'content-type': 'application/json'},
  headers: {'Authorization': Cookies.get("Authorization")},*/
  withCredentials: false,
  baseURL: process.env.NODE_ENV === "production" ? constantName.prod_service_url : constantName.local_service_url , // api的base_url
  timeout: 5000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(config => {
    let token = getToken()
    if(token) {
        config.headers.Authorization = 'Bearer ' + token // admin
    }
    return config
}, error => {
  // alert('baseUrl:' + error.baseUrl + 'url:' + error.url)
  // Do something with request error
  return Promise.reject(error)
})
service.interceptors.response.use(
  response => {
    /**
     * 下面的注释为通过response自定义code来标示请求状态，当code返回如下情况为权限有问题，登出并返回到登录页
     * 如通过xmlhttprequest 状态码标识 逻辑可写在下面error中
     */
    const res = response.data;

    if (res.code !== 200) {
        return Promise.reject('error' + res.msg);
    } else {
      return res;
    }
  },
  error => {
    console.log('err' + error)// for debug
    /*Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })*/
    return Promise.reject(error)
  })

export default service

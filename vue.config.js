/**
 * vue会读取该文件，并合并到webpack配置文件上面，如果你想了解更多关于cli3和webpack
 * https://cli.vuejs.org/zh/guide/webpack.html
 */
const CompressionWebpackPlugin = require("compression-webpack-plugin");
const IS_PRODUCTION = process.env.NODE_ENV === "production";
const path = require('path')
const cdn = {
    css: [],
    js: [

    ],
};
// const host = window.location.host;
console.log(IS_PRODUCTION);
const externals = {
};
const resolve = dir => {
    return path.join(__dirname, dir)
}
module.exports = {
    publicPath: "./",
    outputDir: "dist",
    assetsDir: "static",
    indexPath: "index.html",
    productionSourceMap: false, // 关闭sourceMap
    configureWebpack: {
        // Webpack配置
        // devtool: "none", // webpack内关闭sourceMap
        optimization: {
            // 优化配置
            splitChunks: {
                chunks: "all",
                cacheGroups: {
                    // 拆分Vue
                    vue: {
                        test: /[\\/]node_modules[\\/]vue[\\/]/,
                        name: "chunk-vue",
                    },
                },
            },
        },
        resolve: {
            alias: {
                "@": resolve("src"), // 主目录
                "views": resolve("src/views"), // 页面
                'components': resolve("src/components"), // 组件
                'api': resolve("src/api"), // 接口
                'utils': resolve("src/utils"), // 通用功能
                'assets': resolve("src/assets"), // 静态资源
                'style': resolve("src/style"), // 通用样式
            },
        },
        externals: {
            "BMap": "BMap",
            'BMapLib': 'BMapLib'
        }
    },
    pluginOptions: {
        'style-resources-loader': {
            preProcessor: 'less',
            patterns: [
                resolve('path/to/global.less')
            ]
        }
    },
    chainWebpack(config) {
        if (IS_PRODUCTION) {
            config.plugin("html").tap(args => {
                args[0].cdn = cdn;
                args[0].title = "ST-ADMIN";
                return args;
            });
            config.externals(externals);
            config.plugin("html").tap(args => {
                args[0].minify.minifyCSS = true;
                return args;
            });
            // gzip需要nginx进行配合
            config
                .plugin("compression")
                .use(CompressionWebpackPlugin)
                .tap(() => [
                    {
                        test: /\.js$|\.html$|\.css/, // 匹配文件名
                        threshold: 10240, // 超过10k进行压缩
                        deleteOriginalAssets: true, // 是否删除源文件
                    }
                ]);
        } else {
            config.plugin("html").tap(args => {
                args[0].title = "ST-ADMIN";
                return args;
            });
        }
    },
    css: {
        // 是否使用css分离插件 ExtractTextPlugin
        extract: !!IS_PRODUCTION,
        // 开启 CSS source maps?
        sourceMap: false,
        // css预设器配置项
        // 启用 CSS modules for all css / pre-processor files.
        requireModuleExtension: true,
        loaderOptions: {
            less: {
                lessOptions: {
                    modifyVars: {
                        'primary-color': '#1890ff',                         // 全局主色
                        'link-color': '#1890ff',                            // 链接色
                        'success-color': '#52c41a',                         // 成功色
                        'warning-color': '#faad14',                         // 警告色
                        'error-color': '#f5222d',                           // 错误色
                        'font-size-base': '14px',                           // 主字号
                        'heading-color': 'rgba(0, 0, 0, .85)',              // 标题色
                        'text-color': 'rgba(0, 0, 0, .65)',                 // 主文本色
                        'text-color-secondary ': 'rgba(0, 0, 0, .45)',      // 次文本色
                        'disabled-color ': 'rgba(0, 0, 0, .25)',            // 失效色
                        'border-radius-base': '2px',                        // 组件/浮层圆角
                        'border-color-base': '#d9d9d9',                     // 边框色
                        'box-shadow-base': '0 2px 8px rgba(0, 0, 0, .15)',  // 浮层阴影
                    },
                    javascriptEnabled: true,
                },
            },
        },
    },
    devServer: {
        proxy: {
            '/api': {
                target: 'http://localhost:8001/rest/', //对应自己的接口
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        }
    }
}
